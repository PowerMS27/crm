# CRM for e-commerce

## Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# yarn
yarn install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# yarn
yarn dev
```

## Production

Build the application for production:

```bash
# npm
npm run build

# yarn
yarn build
```

Locally preview production build:

```bash
# npm
npm run preview

# yarn
yarn preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.

## Dependencies
- [Vue 3](https://vuejs.org/) with [Composition API](https://v3.ru.vuejs.org/ru/api/composition-api.html)
- [Nuxt 3](https://nuxt.com/)
- [Pinia](https://pinia.vuejs.org/) - store library and state management framework
- [Vue Router](https://router.vuejs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Tailwind](https://tailwindcss.com/) - css framework
- [Element Plus](https://element-plus.org/en-US/) - component library
- [vue-tsc](https://www.npmjs.com/package/vue-tsc) for type check in console

## Current Routes

`/` main page

`/auth` auth page

## VS Code extensions

#### Main

- `GitLens — Git supercharged`
- `Prettier - Code formatter`
- `Tailwind CSS IntelliSense` - autocomplete for tailwindcss
- `Vue Language Features (Volar)`
- `TypeScript Vue Plugin (Volar)`

#### Optional

- `Auto Rename Tag`
- `Better Comments` - human-friendly comments
- `Material Icon Theme` - just nice icons for folders and files


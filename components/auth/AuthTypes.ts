export type TAuthForm = {
  email: string
  password: string
  isRemember: boolean
}

import type { IOrder } from '@/components/base/BaseTypes'

export type TOrder = IOrder & { filterType: String }

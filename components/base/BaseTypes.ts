export interface IRadioFilter {
  filterType: string
  filterText: string
}
export interface IOrder {
  orderName: string
  companyName: string
  amount: number
  date: string
}
export interface INavLink {
  url: string
  text: string
}

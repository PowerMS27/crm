/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./app.vue",
    "./error.vue",
  ],
  theme: {
    screens: {
      xs: '360px',
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
    },
    colors: {
      'blue': '#409EFF',
      'dark-blue': '#1B2442',
      'deep-blue': '#0A162A',
      'dark-deep-blue': '#0A0F20',
      'light-blue': '#96C9FF',
      'grey': '#C4C4C480',
      'light-grey': '#939393',
      'white': '#FFFFFF'
    },
    fontFamily: {
      contentFont: ['"Montserrat"'],
    },
    extend: {
      spacing: {
        '128': '32rem',
      }
    },
  },
  plugins: [],
}

